<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class ServiceFileUpload extends UploadedFile
{
    private $authorizedType = [
        'jpg', 'JPG', 'jpeg', 'JPEG',
        'png', 'PNG', 'gif', 'GIF',
        'svg', 'SVG'
    ];

    /**
     * ImageUpload constructor.
     * @param $data
     * @throws \Exception
     */
    public function __construct($data)
    {
        if (!$data) {
            throw new \Exception("нет данных.");
        }

        $originalName = $data['name'];
        $base64Content = $data['value'];

        if (!$base64Content) {
            throw new \Exception("нет файла.");
        }

        $mimeType = null;
        $error = null;
        $test = true;

        $filePath = tempnam(sys_get_temp_dir(), "UploadedFile");
        list(, $data) = explode(",", $base64Content);
        $data = base64_decode($data);
        file_put_contents($filePath, $data);

        parent::__construct($filePath, $originalName, $mimeType, $error, $test);
    }

    /**
     * @param string $dir
     * @return string
     * @throws \Exception
     */
    public function upload($dir = "./")
    {
        if (!in_array($this->guessExtension(), $this->authorizedType)) {
            throw new \Exception("неверый тип данных.");
        }

        $fileName = md5(uniqid()) . '.' . $this->guessExtension();
        $this->move($dir, $fileName);
        return $fileName;
    }


}