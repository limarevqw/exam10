<?php

namespace App\Libs\Helpers;

class CloningPhotos
{
    public static function getName($path = "", $sourcePath = "")
    {
        if (!$sourcePath) {
            $sourcePath = './public/featuresImages/'.$path."/";
        } else {
            $sourcePath = './public/featuresImages/'.$sourcePath;
        }
        $filesNames = self::getListOfFileNamesFromTheFolder($sourcePath);
        $imageRandomPath = $sourcePath.$filesNames[array_rand($filesNames)];
        $mine = trim(substr($imageRandomPath, strrpos($imageRandomPath, '.'), 5));
        $fileName = sha1(microtime(true)).$mine;
        $fullPath = "./public/uploads/".$path."/";
        $newFullPath = $fullPath.$fileName;

        @mkdir($fullPath,0777);
        @copy($imageRandomPath, $newFullPath);

        return $fileName;
    }

    public static function removeDirectory($dir)
    {
        if ($objs = glob($dir."/*")) {
            foreach($objs as $obj) {
                is_dir($obj) ? self::removeDirectory($obj) : unlink($obj);
            }
        }
        @rmdir($dir);
    }

    public static function getListOfFileNamesFromTheFolder($dir, $sort = 0)
    {
        $list = scandir($dir, $sort);

        if (!$list) {
            return false;
        }

        if ($sort == 0) {
            unset($list[0], $list[1]);
        } else {
            unset($list[count($list) - 1], $list[count($list) - 1]);
        }

        return $list;
    }
}