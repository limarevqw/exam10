<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $articles = ArticleFixtures::getArticles();

        /** @var User $user */
        $user = $this->getReference(UserFixtures::USER_ONE);

        $faker = Factory::create();

        for ($i = 1; $i <= 120; $i++) {

            /** @var Article $article */
            $article = $this->getReference($articles[array_rand($articles)]->getTitle());

            $comment = new Comment();
            $comment
                ->setCreatedAt(new \DateTime())
                ->setContent($faker->text(300))
                ->setArticle($article)
                ->setUser($user)
            ;

            $manager->persist($comment);
        }

        $manager->flush();

    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
            ArticleFixtures::class
        ];
    }
}