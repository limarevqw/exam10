<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Heading;
use App\Entity\Tag;
use App\Entity\User;
use App\Libs\Helpers\CloningPhotos;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ArticleFixtures extends Fixture implements DependentFixtureInterface
{

    private static $articles = [];

    protected $container;

    public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        CloningPhotos::removeDirectory($this->container->getParameter("article_directory"));
        /** @var User $user */
        $user = $this->getReference(UserFixtures::USER_ONE);
        $headings = HeadingFixtures::getHeadings();
        $tags = TagsFixtures::getHeadings();
        $faker = Factory::create();

        for ($i = 1; $i <= 100; $i++) {

            /** @var Heading $heading */
            $heading = $this->getReference($headings[array_rand($headings)]->getName());
            /** @var Tag $tag */
            $tag = $this->getReference($tags[array_rand($tags)]->getName());
            $thumbName = CloningPhotos::getName("article");

            $article = new Article();
            $article
                ->setCreatedAt(new \DateTime())
                ->setUpdatedAt(new \DateTime())
                ->setTitle($faker->text(200))
                ->setDescription($faker->text)
                ->setContent($faker->text(2000))
                ->setUser($user)
                ->setThumb($thumbName)
                ->setHeading($heading)
            ;

            if ($i > 15 ) {
                $article->setActive(true);
                $article->setDatePublication(new \DateTime());
            }

            $article->addTagArticle($tag);
            $tag->addArticle($article);
            $user->addArticle($article);

            $manager->persist($article);
            $manager->persist($tag);
            $manager->persist($user);

            self::$articles[] = $article;

            $this->addReference($article->getTitle(), $article);
        }

        $manager->flush();

    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
            HeadingFixtures::class,
            TagsFixtures::class,
        ];
    }

    /**
     * @return Article[]
     */
    public static function getArticles()
    {
        return self::$articles;
    }
}