<?php

namespace App\DataFixtures;

use App\Entity\ImageGallery;
use App\Entity\Article;
use App\Libs\Helpers\CloningPhotos;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImageGalleryFixtures extends Fixture implements DependentFixtureInterface
{
    protected $container;

    public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        CloningPhotos::removeDirectory($this->container->getParameter("image_gallery_directory"));
        $articles = ArticleFixtures::getArticles();

        for ($i = 1; $i <= 100; $i++) {
            /** @var Article $article */
            $article = $this->getReference($articles[array_rand($articles)]->getTitle());

            $imageGallery = new ImageGallery();
            $imgName = CloningPhotos::getName("image_gallery", "/article/");

            $imageGallery
                ->setCreatedAt(new \DateTime())
                ->setUpdatedAt(new \DateTime())
                ->setArticle($article)
                ->setName($imgName)
            ;

            $manager->persist($imageGallery);

        }

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            ArticleFixtures::class
        ];
    }
}