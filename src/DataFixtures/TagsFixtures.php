<?php

namespace App\DataFixtures;

use App\Entity\Tag;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class TagsFixtures extends Fixture implements DependentFixtureInterface
{
    private static $tags = [];

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /** @var User $user */
        $user = $this->getReference(UserFixtures::USER_ONE);
        $faker = Factory::create();

        for ($i = 1; $i <= 30; $i++) {

            $tag = new Tag();
            $tag
                ->setCreatedAt(new \DateTime())
                ->setUpdatedAt(new \DateTime())
                ->setActive(true)
                ->setName($faker->name(15))
                ->setUser($user)
            ;

            self::$tags[] = $tag;

            $this->addReference($tag->getName(), $tag);

            $manager->persist($tag);
        }

        $manager->flush();

    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }

    /**
     * @return Tag[]
     */
    public static function getHeadings()
    {
        return self::$tags;
    }
}