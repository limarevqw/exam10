<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\Heading;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * @param bool $active
     * @param Heading|null $heading
     * @param null $tag
     * @return Article[] Returns an array of Article objects
     */
    public function findByPublication($active = true, Heading $heading = null, $tag = null)
    {
        $query = $this->createQueryBuilder('a')
            ->andWhere("a.active = :active")
            ->setParameter(":active", $active)

        ;

        if ($active) {
            $query->andWhere("a.DatePublication <= :DatePublication");
            $query->setParameter(":DatePublication", new \DateTime());
        }

        if ($heading) {
            $query
                ->andWhere("a.heading = :heading")
                ->setParameter(":heading", $heading)
            ;
        }

        if ($tag) {
            $query
                ->join("a.tags", "tags")
                ->andWhere("tags.id IN(:tag)")
                ->setParameter(":tag", $tag)
            ;
        }

        return $query
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }
}
