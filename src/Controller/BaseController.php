<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Heading;
use App\Entity\Tag;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class BaseController extends Controller
{
    protected $request;

    public function __construct(ContainerInterface $container)
    {
        $this->request = $container->get('request_stack')->getCurrentRequest();
    }

    public function render(string $view, $parameters = array(), Response $response = null): Response
    {
        $data = array_merge($this->getCommonData(), $parameters);
        return parent::render($view, $data, $response);
    }

   public function getCommonData()
   {
       $headingSession = $this->request->getSession()->get("heading");
       $tagsSession = $this->request->getSession()->get("tags");

       $headingRepository = $this->getDoctrine()->getRepository(Heading::class);
       $tagsRepository = $this->getDoctrine()->getRepository(Tag::class);

       /** @var Heading[] $headings */
       $headings = $headingRepository->findAll();

       /** @var Tag[] $headings */
       $tags = $tagsRepository->findAll();

        return [
            'headings' => $headings,
            'tags' => $tags,
            'headingSession' => $headingSession,
            'tagsSession' => json_encode($tagsSession),
        ];
   }

   public function getPaginatorArticles(
       $active = true,
       Request $request,
       ArticleRepository $articleRepository,
       $heading = null,
       $tag = null
   )
   {
       $articles = $articleRepository->findByPublication($active, $heading, $tag);

       $paginator = $this->get('knp_paginator');
       $page = $request->query->getInt("page", 1);

       /** @var Article[] $articlesResult */
       $articlesResult = $paginator->paginate(
           $articles,
           $page,
           $request->query->getInt("limit", 7)
       );

       return $articlesResult;
   }
}