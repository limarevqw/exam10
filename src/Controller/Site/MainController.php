<?php

namespace App\Controller\Site;

use App\Controller\BaseController;
use App\Repository\ArticleRepository;
use App\Repository\HeadingRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MainController
 * @package App\Controller\Site
 */
class MainController extends BaseController
{
    /**
     * @Route("/", name="site_main_index")
     * @param ArticleRepository $articleRepository
     * @param HeadingRepository $headingRepository
     * @param Request $request
     * @return Response
     */
    public function indexAction(
        ArticleRepository $articleRepository,
        HeadingRepository $headingRepository,
        Request $request
    )
    {

        $headingSession = $request->getSession()->get("heading");
        $tags = $request->getSession()->get("tags");

        $heading = null;

        if ($headingSession) {
            $heading = $headingRepository->find(intval($headingSession));
        }


        return $this->render("site/main/index.html.twig", [
            'articles'  => $this->getPaginatorArticles(true, $request, $articleRepository, $heading, $tags),
            'title' => 'Список новостей'
        ]);
    }

    /**
     * @Route("/set-filters", name="site_main_set_filters")
     * @param Request $request
     * @return JsonResponse
     */
    public function setFiltersAction(Request $request)
    {
        $status = true;
        $message = null;

        try {
            $session = new Session();
            $session->set("heading", $request->get("filters-heading"));
            $session->set("tags", $request->get("filters-tags"));

            $message = "ok";

        } catch (\Exception $e) {
            $status = false;
            $message = $e->getMessage();
        }

        return new JsonResponse([
            'status' => $status,
            'message' => $message
        ]);
    }

    /**
     * @Route("/reset-filters", name="site_main_reset_filters")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function resetFiltersAction(Request $request)
    {
        $request->getSession()->clear();
        return $this->redirectToRoute("site_main_index");
    }
}
