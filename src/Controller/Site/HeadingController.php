<?php

namespace App\Controller\Site;

use App\Controller\BaseController;
use App\Repository\ArticleRepository;
use App\Repository\HeadingRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HeadingController
 * @package App\Controller\Site
 * @Route("heading")
 */
class HeadingController extends BaseController
{
    /**
     * @Route("/{id}", name="site_heading_index")
     * @param ArticleRepository $articleRepository
     * @param HeadingRepository $headingRepository
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(
        ArticleRepository $articleRepository,
        HeadingRepository $headingRepository,
        Request $request,
        $id
    )
    {
        $heading = $headingRepository->find(intval($id));

        return $this->render("site/main/index.html.twig", [
            'articles'  => $this->getPaginatorArticles(true, $request, $articleRepository, $heading),
            'title' => 'Список новостей'
        ]);
    }

}