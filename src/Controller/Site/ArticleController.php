<?php
namespace App\Controller\Site;

use App\Controller\BaseController;
use App\Entity\Article;
use App\Entity\ImageGallery;
use App\Repository\ArticleRepository;
use App\Repository\HeadingRepository;
use App\Repository\TagRepository;
use App\Service\ServiceFileUpload;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ArticleController
 * @package App\Controller\Site
 * @Route("article")
 *
 */
class ArticleController extends BaseController
{

    /**
     * @Route("/", name="site_article_index")
     * @return Response
     */
    public function indexAction()
    {
        return $this->render("site/article/index.html.twig");
    }

    /**
     * @Route("/moderation/no-publication", name="site_article_noPublication")
     * @param Request $request
     * @param ArticleRepository $articleRepository
     * @return Response
     */
    public function noPublicationAction(
        Request $request,
        ArticleRepository $articleRepository
    )
    {
        return $this->render("site/main/index.html.twig", [
            'title' => 'статьи на модерацию',
            'articles'  => $this->getPaginatorArticles(false, $request, $articleRepository),
        ]);
    }

    /**
     * @Route("/{id}/show", name="site_article_show")
     * @param ArticleRepository $articleRepository
     * @return Response
     */
    public function showAction(ArticleRepository $articleRepository, $id)
    {
        return $this->render("site/article/show.html.twig", [
            'article' => $articleRepository->find(intval($id))
        ]);
    }

    /**
     * @Route("/moderation/create", name="site_article_create")
     * @param HeadingRepository $headingRepository
     * @param TagRepository $tagRepository
     * @return Response
     */
    public function createAction(
        HeadingRepository $headingRepository,
        TagRepository $tagRepository
    )
    {
        $headings = $headingRepository->findAll();
        $tags = $tagRepository->findAll();
        return $this->render("site/article/create.html.twig", [
            'headings' => $headings,
            'tags' => $tags,
        ]);
    }

    /**
     * @Route("/moderation/add", name="site_article_add")
     * @param Request $request
     * @param ObjectManager $manager
     * @param HeadingRepository $headingRepository
     * @param TagRepository $tagRepository
     * @return Response
     */
    public function addAction(
        Request $request,
        ObjectManager $manager,
        HeadingRepository $headingRepository,
        TagRepository $tagRepository
    )
    {
        $status = true;
        $message = null;
        $thumbName = null;
        $lastId = null;
        $fileDirName = $this->getParameter('article_directory');

        $manager->getConnection()->beginTransaction();

        try {

            if (!$request->isXmlHttpRequest()) {
                throw new \Exception("Это не ajax", 007);
            }

            $formData = $request->get("formData");
            $thumbData = $request->get("thumbData");

            if (!empty($thumbData)) {
                $file = new ServiceFileUpload($thumbData[0]);
                $thumbName = $file->upload($fileDirName);
            }

            $heading = $headingRepository->find($formData['heading']);
            $tags = $tagRepository->findSelectIdTags($formData['tags']);

            $article = new Article();

            $article
                ->setUser($this->getUser())
                ->setTitle($formData["title"])
                ->setThumb($thumbName ?? "")
                ->setDescription($formData['description'])
                ->setContent($formData['content'])
                ->setHeading($heading)
                ->setDatePublication(null)
            ;

            if ($tags) {
                foreach ($tags as $tag) {
                    $tag->addArticle($article);
                    $manager->persist($tag);
                    $article->addTagArticle($tag);
                }
            }

            $manager->persist($article);
            $manager->flush();

            $lastId = $article->getId();

            $message = "Статья - " . $article->getTitle() . " добавлена.";

            $manager->commit();


        } catch (\Exception $e) {
            $manager->getConnection()->rollBack();

            if ($thumbName) {
                $fullPath = $fileDirName . "/" . $thumbName;

                if (file_exists($fullPath)) {
                    unlink($fullPath);
                }
            }

            $status = false;
            $message = $e->getMessage();
        }

        return new JsonResponse([
            'status' => $status,
            'message' => $message,
            'lastId' => $lastId
        ]);
    }

    /**
     * @Route("/moderation/{id}/publication", name="site_article_publication")
     * @param Request $request
     * @param ArticleRepository $articleRepository
     * @param ObjectManager $manager
     * @param $id
     * @return JsonResponse
     */
    public function publicationAction(
        Request $request,
        ArticleRepository $articleRepository,
        ObjectManager $manager,
        $id
    )
    {
        $status = true;
        $message = null;
        $dateTime = new \DateTime();

        $article = $articleRepository->find(intval($id));

        try {

            $publication = $request->get("publication");

            if ($publication) {
                $dateTime = new \DateTime($publication);

            }

            if (!$article) {
                throw new \Exception("Запись не найдена.");
            }

            if (!$this->getUser()) {
                throw new \Exception("Сначало надо авторизоваться ", 007);
            }

            $article->setActive(true);
            $article->setDatePublication($dateTime);
            $manager->persist($article);
            $manager->flush();

            $message = "Запись успешно опубликована дата назначена ".$publication;

        } catch (\Exception $e) {
            $status = false;
            $message = $e->getMessage();
        }

        return new JsonResponse([
            'status' => $status,
            'message' => $message
        ]);
    }

    /**
     * @Route("/upload_image", name="site_article_upload_image")
     * @Method({"POST"})
     * @param Request $request
     * @param ArticleRepository $articleRepository
     * @param ObjectManager $manager
     * @return Response
     */
    public function uploadGalleryAction(
        Request $request,
        ArticleRepository $articleRepository,
        ObjectManager $manager
    )
    {
        $status = true;
        $message = null;
        $imageName = null;

        $fileDirName = $this->getParameter('image_gallery_directory');

        try {

            $imageData = $request->get('data');
            $id = $request->get('id_item');

            if (!$imageData) {
                throw new \Exception("файл не был передан.");
            }

            $article = $articleRepository->find(intval($id));

            if (!$article) {
                throw new \Exception("Запись не найдена");
            }

            $file = new ServiceFileUpload($imageData);
            $imageName = $file->upload($fileDirName);

            if (!$imageName) {
                throw new \Exception("Ошибка загрузки файла.");
            }

            $gallery = new ImageGallery();

            $gallery
                ->setArticle($article)
                ->setName($imageName)
            ;

            $manager->persist($gallery);
            $manager->flush();

        } catch (\Exception $e) {
            $status = false;
            $message = $e->getMessage();
        }

        return new JsonResponse([
            'status' => $status,
            'message' => $message
        ]);
    }


}