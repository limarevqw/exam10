<?php

namespace App\Controller\Site;

use App\Controller\BaseController;
use App\Entity\Comment;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CommentController
 * @package App\Controller\Site
 * @Route("comment")
 */
class CommentController extends BaseController
{

    /**
     * @Route("/{id}/add", name="site_comment_add")
     * @param Request $request
     * @param ObjectManager $manager
     * @param ArticleRepository $articleRepository
     * @param null $id
     * @return JsonResponse
     */
    public function addAction(
        Request $request,
        ObjectManager $manager,
        ArticleRepository $articleRepository,
        $id = null
    )
    {
        $status = true;
        $message = null;
        $commentContent = null;

        try {

            if (!$request->isXmlHttpRequest()) {
                throw new \Exception("Это не ajax", 007);
            }

            if (!$this->getUser()) {
                throw new \Exception("Сначало надо авторизоваться ", 007);
            }

            $formData = $request->get("formData");

            $article = $articleRepository->find(intval($id));

            if (!$article) {
                throw new \Exception("Статья не найденна", 007);
            }

            $comment = new Comment();
            $comment
                ->setUser($this->getUser())
                ->setArticle($article)
                ->setContent($formData['content'])
            ;

            $manager->persist($comment);
            $manager->flush();

            $message = "Комент добавлен.";
            $commentContent = $this->render("site/comment/list.html.twig", [
                'comment' => $comment
            ])->getContent();

        } catch (\Exception $e) {
            $status = false;
            $message = $e->getMessage();
        }

        return new JsonResponse([
            'status' => $status,
            'message' => $message,
            'commentContent' => $commentContent
        ]);
    }

    /**
     * @Route("/moderation/{id}/delete", name="site_comment_delete")
     * @param CommentRepository $commentRepository
     * @param ObjectManager $manager
     * @param $id
     * @return JsonResponse
     */
    public function deleteAction(
        CommentRepository $commentRepository,
        ObjectManager $manager,
        $id
    )
    {
        $status = true;
        $message = null;

        $comment = $commentRepository->find(intval($id));

        try {

            if (!$comment) {
                throw new \Exception("Запись не найдена.");
            }

            if (!$this->getUser()) {
                throw new \Exception("Сначало надо авторизоваться ", 007);
            }

            $manager->remove($comment);
            $manager->flush();

            $message = "Запись успешно удалена";

        } catch (\Exception $e) {
            $status = false;
            $message = $e->getMessage();
        }

        return new JsonResponse([
            'status' => $status,
            'message' => $message
        ]);
    }
}