<?php

namespace App\Controller\Site;

use App\Controller\BaseController;
use App\Repository\ArticleRepository;
use App\Repository\TagRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HeadingController
 * @package App\Controller\Site
 * @Route("tag")
 */
class TagController extends BaseController
{
    /**
     * @Route("/{id}", name="site_tag_index")
     * @param ArticleRepository $articleRepository
     * @param TagRepository $tagRepository
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(
        ArticleRepository $articleRepository,
        TagRepository $tagRepository,
        Request $request,
        $id
    )
    {
        $tags = $tagRepository->find(intval($id));

        return $this->render("site/main/index.html.twig", [
            'articles'  => $this->getPaginatorArticles(true, $request, $articleRepository, null, $tags),
            'title' => 'Список новостей'
        ]);
    }

}