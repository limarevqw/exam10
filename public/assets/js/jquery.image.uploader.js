//http://alexvaleev.ru/javascript-optimizator/


/**
 *
 * @param params
 * @constructor
 */
function JqueryImageUploader(params = []) {

    /**
     * @param params.mainBlockName
     * @param params.dataArray = []
     * @param params.maxFiles = 15
     * @param params.maxSizeFile = 10000000
     * @param params.thumb = false
     * @param params.selectMain = false
     */

    this.thumb = params.thumb ? params.thumb : false;
    this.selectMain = params.selectMain ? params.selectMain : false;
    this.dataArrayDel = [];
    this.dataArray =  params.dataArray ? params.dataArray : [];
    this.maxFiles = params.maxFiles ? params.maxFiles : 15;
    this.maxSizeFile = params.maxSizeFile ? params.maxSizeFile : 10000000;
    this.urlUploadImage = params.urlUploadImage ? params.urlUploadImage : null;
    this.errMessage = 0;
    this.btnNameMainImage = params.mainBlockName + ' .main-button';
    this.$mainBlockName = $(params.mainBlockName);
    this.$defaultUploadBtn = $(params.mainBlockName + ' #uploadbtn');
    this.$loadingBarColor = $(params.mainBlockName + ' .loading-color');
    this.$loading = $(params.mainBlockName + ' #loading');
    this.$loadingContent = $(params.mainBlockName + ' #loading-content');
    this.$uploadButton = $(params.mainBlockName + ' #upload-button');
    this.$uploadButtonSpan = $(params.mainBlockName + ' #upload-button span');
    this.droppedFilesImageName = '#dropped-files > .image';
    this.$droppedFiles = $(params.mainBlockName+' #dropped-files');
    this.$droppedFilesMessage = $(params.mainBlockName + ' #drop-files p');
    this.$dropfiles = $(params.mainBlockName+' #drop-files');
    this.$uploadedHolder = $(params.mainBlockName + ' #uploaded-holder');
    this.$mainImg = $(params.mainBlockName + ' .main_img');
    this.$frm = $(params.mainBlockName + ' #frm');
    this.$deleteAllImages = $(params.mainBlockName + ' #dropped-files #upload-button .delete');

    this.randomNumber = function(min, max) {
        return 'query-558878778878745' + Math.floor(Math.random() * (max - min + 1)) + min;
    };

    this.restartFiles = function () {

        let currentObject = this;

        currentObject.$deleteAllImages.on('click', function () {
            // Установим бар загрузки в значение по умолчанию
            currentObject.$loadingBarColor.css({'width' : '0%'});
            currentObject.$loading.css({'display' : 'none'}).html('');

            // Удаляем все изображения на странице и скрываем кнопки
            currentObject.$uploadButton.hide();
            currentObject.$mainBlockName.find(currentObject.droppedFilesImageName).remove();
            currentObject.$uploadedHolder.hide();
            currentObject.$mainImg.val(0);

            // Очищаем массив
            currentObject.dataArray.length = 0;
            return false;
        });


    };

    this.addImage = function (ind) {

        let current_main = this.$mainImg.val();
        let start, end;
        let dataArray = this.dataArray;


        // Если индекс отрицательный значит выводим весь массив изображений
        if (ind < 0 ) {
            start = 0;
            end = dataArray.length;
        } else {
            // иначе только определенное изображение
            start = ind; end = ind + 1;
        }

        // Оповещения о загруженных файлах
        if(dataArray.length === 0) {
            // Если пустой массив скрываем кнопки и всю область
            this.$uploadButton.hide();
            this.$uploadedHolder.hide();
        } else if (dataArray.length === 1) {
            this.$uploadButtonSpan.html("Был выбран 1 файл");
        } else {
            this.$uploadButtonSpan.html(dataArray.length+" файлов ");
        }

        // Цикл для каждого элемента массива
        for (let i = start; i < end; i++) {
            // размещаем загруженные изображения
            if(this.$mainBlockName.find(this.droppedFilesImageName).length <= this.maxFiles) {
                this.$droppedFiles.append(this.getTemplateImage(dataArray[i], i));
            }
        }

        if (current_main === '0') {
            this.getMainImg(0).addClass('main-button-active').html('Главная');
            this.$mainImg.val(0);
        } else {
            this.getMainImg(current_main).addClass('main-button-active').html('Главная');
        }

        return false;
    };

    this.getMainImg = function (number) {
        return this.$mainBlockName.find("a[data-main-id_pic^='" + number + "']");
    };

    this.loadInView = function (files) {

        if (this.thumb) {
            this.$mainBlockName.find(this.droppedFilesImageName).remove();
            this.dataArray.length = 0;
        }

        let currentObject = this;
        let dataArray = currentObject.dataArray;
        let errMessage = currentObject.errMessage;
        // Показываем обасть предпросмотра
        this.$uploadedHolder.show();

        // Для каждого файла
        $.each(files, function(index, file) {

            // Несколько оповещений при попытке загрузить не изображение
            if (!files[index].type.match('image.*')) {

                if(errMessage === 0) {
                    currentObject.$droppedFilesMessage.html('Эй! только изображения!');
                    ++errMessage
                } else if(errMessage === 1) {
                    currentObject.$droppedFilesMessage.html('Стоп! Загружаются только изображения!');
                    ++errMessage
                } else if(errMessage === 2) {
                    currentObject.$droppedFilesMessage.html("Не умеешь читать? Только изображения!");
                    ++errMessage
                } else if(errMessage === 3) {
                    currentObject.$droppedFilesMessage.html("Хорошо! Продолжай в том же духе");
                    errMessage = 0;
                }
                return false;
            }

            // Проверяем количество загружаемых элементов
            if((dataArray.length + files.length) <= currentObject.maxFiles) {
                // показываем область с кнопками
                if (currentObject.thumb) {
                    currentObject.$uploadButton.hide();
                } else {
                    currentObject.$uploadButton.show();
                }

            } else {
                alert('Вы не можете загружать больше '+currentObject.maxFiles+' изображений!');
                return;
            }

            // Создаем новый экземпляра FileReader
            let fileReader = new FileReader();
            // Инициируем функцию FileReader
            fileReader.onload = (function(file) {
                if (file.size < currentObject.maxSizeFile) {
                    return function() {
                        // Помещаем URI изображения в массив
                        dataArray.push({
                            name: file.name,
                            value: this.result,
                            id_pic: currentObject.randomNumber(1,12548),
                            short_tag: '',
                            old: 0,
                            type_img: 0
                        });
                        currentObject.addImage((dataArray.length-1));
                    };
                } else {
                    alert('Изображения с именем '+file.name+' привышает допустимый размер 3м');

                }

            })(files[index]);
            // Производим чтение картинки по URI
            fileReader.readAsDataURL(file);
        });

        return false;
    };

    this.getTemplateImage = function (dataArray, i) {
        let template = '<div class="image" style="background: url('+dataArray.value+'); background-size: cover;" data-id="'+i+'">';

        if (!this.thumb) {
            if (this.selectMain) {
                template += '<a href="javascript:void(0);" data-main="main_image" data-main-id_pic="'+dataArray.id_pic+'" class="main-button">Сделать главной</a>';
            }
        }
        //template +=  '<a id="short-'+i+'"  class="short-tags-button"><input type="text" class="short-tags" iid="'+dataArray.id_pic+'" placeholder="введите тег" value="'+dataArray.short_tag+'"></a>';
        template += '<a href="javascript:void(0);" data-delete="drop" style="cursor: pointer;" data-id="'+i+'" data-id_pic="'+dataArray.id_pic+'" data-old="'+dataArray.old+'" class="drop-button">Удалить изображение</a>';
        template += '</div>';

        return template;
    };

    this.deleteImage = function () {

        let currentObject = this;

        // Удаление только выбранного изображения
        currentObject.$droppedFiles.on("click", "a[data-delete^='drop']", function() {

            let id = $(this).data('id');
            let id_pic = $(this).data('id_pic');
            let current_main = currentObject.$mainImg.val();

            let old = $(this).attr('old');
            if (old === 1) {
                currentObject.dataArrayDel.push({gallery_id : id_pic});
            }

            if (id_pic === current_main) {
                currentObject.$mainImg.val(0);
            }

            /* // делим строку id на 2 части
             $(main_block_name+" .short-tags").map(function(index, element){
                 currentObject.dataArray[index].short_tag = $(element).val();
             });*/
            // получаем значение после тире тоесть индекс изображения в массиве
            currentObject.dataArray.splice(id,1);
            // Удаляем старые эскизы
            currentObject.$mainBlockName.find(currentObject.droppedFilesImageName).remove();
            // Обновляем эскизи в соответсвии с обновленным массивом
            currentObject.addImage(-1);
        });
    };

    this.setMainImage = function () {

        let currentObject = this;
        currentObject.$droppedFiles.on("click", "a[data-main^='main_image']", function() {
            $(currentObject.btnNameMainImage).removeClass('main-button-active').html('Сделать главной');

            $(this).addClass('main-button-active').html('Главная');
            currentObject.$mainImg.val($(this).data('main-id_pic'));
        });
    };

    this.dataDropAction = function () {

        let currentObject = this;

        $(function ($) {
            $.event.props.push('dataTransfer');
        });

        currentObject.$dropfiles.on('dragover', function() {
            return false;
        });

        // Метод при падении файла в зону загрузки
        currentObject.$dropfiles.on('drop', function(e) {
            // Передаем в files все полученные изображения
            let files = e.dataTransfer.files;
            // Проверяем на максимальное количество файлов
            if (files.length <= currentObject.maxFiles) {
                // Передаем массив с файлами в функцию загрузки на предпросмотр
                currentObject.loadInView(files);
                return false;
            } else {
                alert('Вы не можете загружать больше '+currentObject.maxFiles+' изображений!');
                files.length = 0;
            }
        });

        // При нажатии на кнопку выбора файлов
        currentObject.$defaultUploadBtn.on('change', function() {

            let files = $(this)[0].files;
            // Проверяем на максимальное количество файлов
            if (files.length <= currentObject.maxFiles) {
                // Передаем массив с файлами в функцию загрузки на предпросмотр
                currentObject.loadInView(files);
                // Очищаем инпут файл путем сброса формы
                currentObject.$frm.each(function(){
                    this.reset();
                });
            } else {
                alert('Вы не можете загружать больше '+currentObject.maxFiles+' изображений!');
                files.length = 0;
            }
        });

        // Простые стили для области перетаскивания
        currentObject.$dropfiles.on('dragenter', function() {
            $(this).css({'box-shadow' : 'inset 0px 0px 20px rgba(0, 0, 0, 0.1)', 'border' : '2px dashed #bb2b2b'});
            return false;
        });
    };

    this.uploadLoadingItemImg = function (idItem = null, callback) {

        if (this.dataArray.length > 0) {
            let dataArray = this.dataArray;
            let urlUploadImage = this.urlUploadImage;
            let totalPercent = 100 / dataArray.length;
            let $loadingContent = this.$loadingContent;
            let $loading = this.$loading;
            let $loadingBarColor = this.$loadingBarColor;
            let x = 0;

            $loading.slideDown();
            $loadingContent.html('Идет Загрузка пожалуйта подождите...');

            for (let i = 0; i < dataArray.length; i++) {

                $.post(urlUploadImage, {id_item:idItem, data:dataArray[i]})
                    .done(function () {

                        $loadingBarColor.css("background", "#5cb85c");
                        ++x;

                        $loadingBarColor.css({ 'width': totalPercent * (x) + '%' });

                        if (totalPercent * (x) === 100) {
                            // Загрузка завершена
                            $loadingContent.html('Загрузка завершена!');

                            callback();

                        } else if (totalPercent * (x) < 100) {
                            $loadingContent.html('Загружается ' + dataArray[x].name + ' Пожалуйта подождите..');
                        }
                    })
                    .fail(function () {
                        ++x;
                        $loadingBarColor.css("background", "red");
                        $loadingContent.html('Ошибка при загрузки ');
                        alert('Ошибка при загрузки ');
                    });
            }
        } else {
            callback();
        }

    };

    this.setDataImage  =  function (data) {

        let currentObject = this;
        this.$uploadedHolder.show();

        currentObject.dataArray.push({
            name: data.name,
            value: data.fullPath,
            id_pic: data.id,
            short_tag: '',
            old: 1,
            type_img: 0
        });

        currentObject.addImage((currentObject.dataArray.length-1));
    };

    this.init = function () {

        if (this.thumb) {
            this.$mainBlockName.find(".content-img-upload").show().addClass("content-img-upload-thumb");
        } else {
            this.$defaultUploadBtn.attr("multiple", "multiple");
            this.$mainBlockName.find(".content-img-upload").show();
        }

        this.dataDropAction();
        this.restartFiles();
        this.setMainImage();
        this.deleteImage();
    }
}